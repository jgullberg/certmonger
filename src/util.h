/*
 * Copyright (C) 2009 Red Hat, Inc.
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef util_h
#define util_h

char *read_config_file(const char *filename);
char *get_config_entry(char *data, const char *section, const char *key);

/*
 * Convert string to upper case in place.
 * String must be null-terminated.  Locale-unaware.
 */
void str_to_upper_inplace(char *s);

/*
 * Return upper-cased copy of string.
 * String must be null-terminated.  Locale-unaware.
 * Return NULL on error (insufficient memory).
 */
char *str_to_upper(const char *s);

#endif
