Source: certmonger
Section: utils
Priority: optional
Maintainer: Debian FreeIPA Team <pkg-freeipa-devel@alioth-lists.debian.net>
Uploaders: Timo Aaltonen <tjaalton@debian.org>
Build-Depends: debhelper-compat (= 13), quilt,
 autopoint,
 dbus (>= 1.8),
 dos2unix,
 expect,
 libdbus-1-dev,
 libcurl4-openssl-dev,
 libidn2-dev,
 libjansson-dev,
 libkrb5-dev,
 libldap2-dev,
 libnspr4-dev,
 libnss3-tools,
 libnss3-dev (>= 2:3.69),
 libpopt-dev,
 libssl-dev,
 systemd [linux-any],
 libtevent-dev,
 libxml2-dev,
 lsb-release,
 openssl,
 pkg-config,
 uuid-dev,
Standards-Version: 4.6.0
Homepage: https://pagure.io/certmonger/
Vcs-Git: https://salsa.debian.org/freeipa-team/certmonger.git
Vcs-Browser: https://salsa.debian.org/freeipa-team/certmonger

Package: certmonger
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends},
 dbus,
 nss-plugin-pem,
Description: D-Bus -based service to simplify interaction with certificate authorities
 Certmonger is a D-Bus -based service which attempts to simplify
 interaction with certifying authorities (CAs) on networks which use
 public-key infrastructure (PKI).
 .
 If it knows the location of a certificate, certmonger can track the
 expiration date and notify you when the certificate is about to expire.
 .
 If it has access to the corresponding private key and information about
 the CA which issued the certificate, certmonger can even attempt to
 automatically obtain a new certificate.
 .
 Supports certificate and key storage in PEM or NSSDB formats.
 .
 Can self-sign certificates, or can submit them to either certmaster or
 development versions of IPA.
